﻿
#include <iostream>


class Fraction
{

public:

    Fraction(int InNumerator, int InDenominator)
    {
        if (InDenominator == 0)
        {
            throw "Denominator must not be equal to 0";            
        }

        Numerator = InNumerator;

        Denominator = InDenominator;
    }

private:

    int Numerator;

    int Denominator;
};


int main()
{
    int inputNumerator = 0;
    int inputDenominator = 0;


   std::cout << "Enter two integers: ";
   
  // std::cin >> inputNumerator >> inputDenominator;
   try
   {
       if(std::cin >> inputNumerator >> inputDenominator)
       {

           std::cout << "the input is correct" << std::endl;         
           
       }
       else
       {
          throw "Incorrect input";
       }

       try
       {
           Fraction a(inputNumerator, inputDenominator);
       }
       catch (const char* exception)
       {
           std::cerr << exception << std::endl;
       }
       
   }
   catch (const char* exception)
   {
       std::cerr << exception << std::endl;
       
   }
}


